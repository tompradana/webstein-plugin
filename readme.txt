=== Webstein ===
Contributors: Tommy Pradana
Requires at least: 6.0
Tested up to: 6.5.4
Stable tag: 6.5.4
Requires PHP: 7.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Plugin for Webstein Technical Test

== Description ==

Plugin for Webstein Technical Test

== Screenshots ==

1. Event post type
2. Custom fields, default built-in WodPress custom fields
3. Taxonomy registered to the Even post type

== Changelog ==

= 1.0.0 =
* Initial release.

== How to install ==

1. Login to Your WordPress Admin Area
2. Navigate to the Plugins Page. In the left-hand menu, hover over "Plugins" and click "Add New."
3. Click the "Upload Plugin" button at the top of the page. Then, click "Choose File," select the .zip file, and click "Install Now."
4. After installation, click the "Activate" button to enable the plugin on your site.

== Usage ==

1. This plugin contains shortcode function [upcoming_events]
2. The shortcode accepts 6 parameters:
    * number (number)
    * exclude_past_event (yes/no)
    * category (tax slug)
    * orderby (event_date/event_location/event_organizer)
    * order (asc/desc)
    * show_filter (yes/no)

    these parameters are optional

3. Example usage without parameter [upcoming_events]
4. Example usage with parameter [upcoming_events number="10" exclude_past_event="yes" category="webinar" orderby="event_date"]
5. Example usage to enable the filter [upcoming_events show_filter="yes"]