<?php
/**
 * Plugin Name:         Webstein Plugin
 * Plugin URI: 
 * Description:         Plugin for Webstein WordPress test
 * Version:             1.0.0
 * Requires at least:   6.0
 * Requires PHP:        7.4
 * Author:              Tommy Pradana
 * Author URI: 
 * License:             GPLv2
 * License URI:         https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:         webstein-plugin
 * Domain Path:         /languages
 */

// Prevent direct access to the file!
if ( !function_exists( 'add_action' ) ) {
    exit; 
}

/**
 * Register event post type
 * Please read: https://developer.wordpress.org/reference/functions/register_post_type/#description
 * 
 * @return void
 */
function webstein_event_post_type() {
    /**
     * @https://developer.wordpress.org/reference/functions/get_post_type_labels/
     */
    $labels = [
        'name'                  => __( 'Events', 'webstein-plugin' ),
        'singular_name'         => __( 'Event', 'webstein-plugin' ),
        'add_new'               => __( 'Add New Event', 'webstein-plugin' ),
        'add_new_item'          => __( 'Add New Event', 'webstein-plugin' ),
        'new_item'              => __( 'New Event', 'webstein-plugin' ),
        'edit_item'             => __( 'Edit Event', 'webstein-plugin' ),
        'view_item'             => __( 'View Event', 'webstein-plugin' ),
        'view_items'            => __( 'View Events', 'webstein-plugin' ),
        'not_found'             => __( 'No events found', 'webstein-plugin' ),
        'not_found_in_trash'    => __( 'No events found in Trash', 'webstein-plugin' )
    ];
    
    $args = [
        'labels'                => $labels,
        'description'           => __( 'Event post type', 'webstein-plugin' ),
        'public'                => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-calendar-alt',
        'supports'              => ['title', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'author', 'excerpt']
    ];

    // Register the post type
    register_post_type( 'event', $args );

    /**
     * Taxonomy registration
     * See https://developer.wordpress.org/reference/functions/register_taxonomy/
     */
    $taxonomy_args = [
        'labels' => [
            'name'          => __( 'Event Categories', 'webstein-plugin' ),
            'singular_name' => __( 'Event Category', 'webstein-plugin' )
        ],
        'hierarchical'          => true,
        'show_in_quick_edit'    => true,
        'show_admin_column'     => true,
        'rewrite'               => [ 'slug' => 'event-category', 'with_front' => true ]
    ];
    register_taxonomy( 'event_category', 'event', $taxonomy_args );
}
add_action( 'init', 'webstein_event_post_type', 1 );

/**
 * Filters the columns displayed in the Posts list table.
 * See https://developer.wordpress.org/reference/hooks/manage_posts_columns/
 *
 * @param [type] $post_columns
 * @param [type] $post_type
 * @return void
 */
function webstein_event_posts_columns( $columns ) {
    unset($columns['date']);
    $columns['event_date']      = __( 'Event Date', 'webstein-plugin' );
    $columns['event_location']  = __( 'Location', 'webstein-plugin' );
    $columns['event_organizer'] = __( 'Organizer', 'webstein-plugin' );
    $columns['date'] = __( 'Date Published', 'webstein-plugin' );
    return $columns;
}
add_filter( 'manage_event_posts_columns', 'webstein_event_posts_columns', 10, 2 );

/**
 * Fires in each custom column in the Posts list table.
 * See https://developer.wordpress.org/reference/hooks/manage_posts_custom_column/
 *
 * @param [type] $column
 * @param [type] $post_id
 * @return void
 */
function webstein_event_posts_custom_column( $column, $post_id ) {
    switch( $column ) {
        case 'event_date':
        case 'event_location':
        case 'event_organizer':
            echo get_post_meta( $post_id, $column, true );
        break;
    }
}
add_filter( 'manage_event_posts_custom_column', 'webstein_event_posts_custom_column', 10, 2 );

/**
 * Populate options from meta value
 *
 * @param [type] $meta_key
 * @return void
 */
function webstein_event_get_unqiue_meta_values($meta_key) {
    global $wpdb;

    // Prepare the SQL query to select distinct meta values and sort them alphabetically
    $sql = $wpdb->prepare("
        SELECT DISTINCT meta_value, post_id 
        FROM $wpdb->postmeta 
        WHERE meta_key = %s
        ORDER BY meta_value ASC
    ", $meta_key);

    // Execute the query and fetch the results
    $meta_results = $wpdb->get_results($sql);

    // Initialize an array to hold unique meta values
    $unique_meta_values = [];

    // Process results to ensure unique meta values and include post IDs
    foreach ($meta_results as $meta) {
        $meta_value = $meta->meta_value;

        if (!isset($unique_meta_values[$meta_value])) {
            $unique_meta_values[$meta_value] = (object) [
                'meta_value' => $meta_value,
                'post_ids' => []
            ];
        }

        $unique_meta_values[$meta_value]->post_ids[] = $meta->post_id;
    }

    // Convert the associative array to a numeric array
    $unique_meta_values = array_values($unique_meta_values);

    return $unique_meta_values;
}

/**
 * Upcoming events shortcode
 * Plaase read: https://developer.wordpress.org/reference/functions/add_shortcode/
 *
 * @param [type] $atts
 * @return string
 */
function webstein_upcoming_event_shortcode( $atts ) {
    // see: https://developer.wordpress.org/reference/functions/shortcode_atts/
    $atts = shortcode_atts( [
        'number'                => 3,                   // number of events, default: 3
        'exclude_past_event'    => 'no',                // other value: yes, default: noe
        'category'              => '',                  // string separated with comma, default: ''
        'orderby'               => 'event_date',        // other value: 'event_date', 'event_location', 'event_organizer', default: 'event_date'
        'order'                 => 'ASC',               // other value: DESC
        'show_filter'           => 'no',
    ], $atts, 'upcoming_events' );

    // prepare default arguments for event query
    $query_args = [
        'post_type'         => 'event',
        'posts_per_page'    => (int) $atts['number'],
        'post_status'       => 'publish',
        'order'             => esc_attr( $atts['order'] )
    ];

    // orderby
    if ( isset( $atts['orderby'] ) && !empty( $atts['orderby'] ) ) {
        $query_args['meta_key'] = esc_attr( $atts['orderby'] );
        $query_args['orderby']  = 'meta_value';
    } 

    // category filter
    if ( isset( $atts['category'] ) && !empty( $atts['category'] ) ) {
        $query_args['tax_query'] = [
            [
                'taxonomy' => 'event_category',
                'field'    => 'term_id',
                'terms'    => explode( ',', $atts['category'] )
            ]
        ];
    }

    // Exclude past events if specified
    if ( $atts['exclude_past_event'] === 'yes' ) {
        $query_args['meta_query'] = [
            [
                'key'     => 'event_date',
                'value'   => date('Y-m-d'),
                'compare' => '>=',
                'type'    => 'DATE',
            ],
        ];
    }

    // Process filter
    $filter_date = $filter_location = $filter_eo = $filter_cat = "";

    // Filter past or upcoming
    if ( isset( $_GET['event_date'] ) && !empty( $_GET['event_date'] ) ) {
        $filter_date = esc_attr( $_GET['event_date'] );
        if ( $filter_date != 'all' ) {
            if ( $filter_date == 'past' ) {
                $query_args['meta_query'][] = [
                    'key'     => 'event_date',
                    'value'   => date('Y-m-d'),
                    'compare' => '<=',
                    'type'    => 'DATE',
                ];
            } else {
                $query_args['meta_query'][] = [
                    'key'     => 'event_date',
                    'value'   => date('Y-m-d'),
                    'compare' => '>=',
                    'type'    => 'DATE',
                ];
            }
        }
    }

    // Filter by location
    if ( isset( $_GET['event_location'] ) && !empty( $_GET['event_location'] ) ) {
        $filter_location = esc_attr( $_GET['event_location'] );
        if ( $filter_location != 'all' ) {
            $query_args['meta_query'][] = [
                'key'     => 'event_location',
                'value'   => $filter_location,
                'compare' => 'LIKE'
            ];
        }
    }

    // Filter by category
    if ( isset( $_GET['event_category'] ) && !empty( $_GET['event_category'] ) ) {
        $filter_cat = esc_attr( $_GET['event_category'] );
        if ( $filter_cat != 'all' ) {
            $query_args['tax_query'] = [
                [
                    'taxonomy' => 'event_category',
                    'field'    => 'slug',
                    'terms'    => $filter_cat
                ]
            ];
        }
    }

    // Filter by organizer
    if ( isset( $_GET['event_organizer'] ) && !empty( $_GET['event_organizer'] ) ) {
        $filter_eo = esc_attr( $_GET['event_organizer'] );
        if ( $filter_eo != 'all' ) {
            $query_args['meta_query'][] = [
                'key'     => 'event_organizer',
                'value'   => $filter_eo,
                'compare' => 'LIKE'
            ];
        }
    }

    $events = new WP_Query( $query_args );

    ob_start();
    ?>
    <div class="events mt-3">
        <?php if ( $events->have_posts() ) : // check if any events published ?>

            <?php if ( isset( $atts['show_filter'] ) && esc_attr( $atts['show_filter'] ) == 'yes'  ) : ?>
                
                <?php
                    // get event cates
                    $categories = get_terms( [ 'taxonomy' => 'event_category', 'hide_empty' => false ] ); 
                    // get locations
                    $locations  = webstein_event_get_unqiue_meta_values( 'event_location' );
                    // get eos
                    $eos        = webstein_event_get_unqiue_meta_values( 'event_organizer' );
                ?>
                
                <div class="row mb-5">
                    <form id="event-filter-form" action="<?php the_permalink(); ?>" method="GET">
                        <div class="event-filter col d-flex align-items-center justify-content-center ">
                            <div class="select-wrapper">
                                <select name="event_date" onchange="this.form.submit()">
                                    <option value="all"><?php _e( 'All Events', 'webstein-plugin' ); ?></option>
                                    <option value="past" <?php selected( 'past', $filter_date ); ?>><?php _e( 'Past Events', 'webstein-plugin' ); ?></option>
                                    <option value="upcoming" <?php selected( 'upcoming', $filter_date ); ?>><?php _e( 'Upcoming Events', 'webstein-plugin' ); ?></option>
                                </select>
                            </div>

                            <?php if ( !empty( $locations ) ) : ?>
                                <div class="select-wrapper">
                                    <select name="event_location" onchange="this.form.submit()">
                                        <option value="all"><?php _e( 'All Locations', 'webstein-plugin' ); ?></option>
                                        <?php foreach( $locations as $location ) : ?>
                                            <option value="<?php echo esc_html( $location->meta_value ); ?>" <?php selected( esc_attr( $location->meta_value ), $filter_location ); ?>><?php echo esc_html( $location->meta_value ); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>

                            <?php if ( !empty( $eos ) ) : ?>
                                <div class="select-wrapper">
                                    <select name="event_organizer" onchange="this.form.submit()">
                                        <option value="all"><?php _e( 'All Event Organizers', 'webstein-plugin' ); ?></option>
                                        <?php foreach( $eos as $eo ) : ?>
                                            <option value="<?php echo esc_html( $eo->meta_value ); ?>" <?php selected( esc_attr( $eo->meta_value ), $filter_eo ); ?>><?php echo esc_html( $eo->meta_value ); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>

                            <?php if ( !empty( $categories ) ) : ?>
                                <div class="select-wrapper">
                                    <select name="event_category" onchange="this.form.submit()">
                                        <option value="all"><?php _e( 'All Categories', 'webstein-plugin' ); ?></option>
                                        <?php foreach( $categories as $cat ) : ?>
                                            <option value="<?php echo $cat->slug; ?>" <?php selected( $cat->slug, $filter_cat ); ?>><?php echo $cat->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>
                        

                        </div><!-- end .col -->
                    </form><!-- end form -->
                </div><!-- end .row -->

            <?php endif; ?>

            <div class="row">
                <?php while ( $events->have_posts() ) : $events->the_post(); 
                    $location   = get_post_meta( get_the_ID(), 'event_location', true ) != "" ? get_post_meta( get_the_ID(), 'event_location', true ) : __( "Online", "webstein-plugin");
                    $date       = get_post_meta( get_the_ID(), 'event_location', true ) != "" ? get_post_meta( get_the_ID(), 'event_date', true ) : date('Y-m-d');
                    $eo         = get_post_meta( get_the_ID(), 'event_location', true ) != "" ? get_post_meta( get_the_ID(), 'event_organizer', true ) : __( "Event Organizer", "webstein-plugin");
                ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'col-12 col-sm-4' ); ?>>
                        <div class="entry">
                            <?php the_post_thumbnail( 'large', [ 'class' => 'img-fluid entry-thumbnail' ] ); ?>
                            <div class="entry-content p-4 d-flex flex-column">
                                
                                <div class="event-meta mb-1">
                                    <div><i class="fa-solid fa-location-dot"></i> <?php echo esc_html( $location ); ?></div>
                                </div><!-- end .event-meta -->

                                <?php the_title( sprintf( '<h2 class="entry-title mb-3"><a href="%s" title="%s">', esc_url( get_permalink() ), esc_attr( get_the_title() ) ), '</a></h2>' ); ?>

                                <div class="event-meta d-flex">
                                    <div><i class="fa-solid fa-calendar-check"></i> <?php echo esc_html( date( 'F j, Y', strtotime( $date ) ) ); ?></div>
                                    <div><i class="fa-regular fa-font-awesome"></i> <?php echo esc_html( $eo ); ?></div>
                                </div>
                                
                            </div><!-- end .entry-content -->
                        </div><!-- end .entry -->
                    </article><!-- end article -->

                <?php endwhile; ?>
            </div><!-- end .row -->
            
        <?php else : ?>

            <?php if ( isset( $atts['show_filter'] ) && esc_attr( $atts['show_filter'] ) == 'yes'  ) : ?>
                <p class="text-center">
                    <?php esc_html_e( 'There are no events in this criteria :)', 'webstein-theme' ); ?><br/><br/>
                    <a class="button" href="<?php echo remove_query_arg( ['event_date', 'event_location', 'event_organizer', 'event_category' ] ); ?>"><?php _e( 'Reset Filter', 'webstein-plugin' ); ?></a>
                </p>
            <?php else : ?>
                <p class="text-center"><?php esc_html_e( 'There are no events at this time :)', 'webstein-theme' ); ?></p>
            <?php endif; ?>

        <?php endif; wp_reset_postdata(); ?>
    </div><!-- end .events -->

    <?php
    return ob_get_clean();
}
add_shortcode( 'upcoming_events', 'webstein_upcoming_event_shortcode' );